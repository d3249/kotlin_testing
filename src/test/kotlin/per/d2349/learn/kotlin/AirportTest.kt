package per.d2349.learn.kotlin

import io.kotlintest.data.forall
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row
import io.mockk.*
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import per.d3249.learn.kotlin.Airport


class AirportTest : StringSpec() {
    val iah = Airport("IAH", "Houston", true)
    val iad = Airport("IAD", "Dulles", false)
    val ord = Airport("ORD", "Chicago O'Hare", true)

    override fun beforeTest(testCase: TestCase) {
        mockkObject(Airport)
    }

    override fun afterTest(testCase: TestCase, result: TestResult) {
       clearAllMocks()
    }

    init {
        "canary test should pass" {
            true shouldBe true;
        }

        "create Airport" {
            iah.code shouldBe "IAH"
            iad.name shouldBe "Dulles"
            ord.delay shouldBe true
        }

        "sort airport by name"{
            forall(
                row(listOf(), listOf()),
                row(listOf(iad), listOf(iad)),
                row(listOf(iad, iah), listOf(iad, iah)),
                row(listOf(ord, iah, iad), listOf(ord, iad, iah))
            )
            { input, output -> Airport.sort(input) shouldBe output }
        }

        "getAirportData invokes fetchData"{
            every { Airport.fetchData(iad.code) } returns """{"IATA":"IAD","Name":"Dulles", "Delay":false}"""

            Airport.getAirportData(iad.code)

            verify { Airport.fetchData(iad.code) }
        }

        "getAirportData extractss Airport from JSON returned by fetchData" {
            every { Airport.fetchData(iad.code) } returns """{"IATA":"IAD","Name":"Dulles", "Delay":false}"""
            Airport.getAirportData(iad.code) shouldBe iad
        }

        "getAirportData handles error fetching data" {
            val wrongCode = "ERR"
            every { Airport.fetchData(wrongCode) } returns """{}"""

            Airport.getAirportData(wrongCode) shouldBe Airport(wrongCode, "Invalid Airport", false)
        }
    }
}
