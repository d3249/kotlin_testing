package per.d2349.learn.kotlin

import io.kotlintest.data.forall
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.specs.StringSpec
import io.kotlintest.tables.row
import per.d3249.learn.kotlin.Airport

class AirportIntegrationTest : StringSpec() {
    init {
        "fetchData returns response from URL" {
            forall(
                row("IAD", "Dulles"),
                row("SFO", "San Francisco"),
                row("ORD", "Chicago")
            ) { airportCode, partialName ->
                Airport.fetchData(airportCode) shouldContain partialName
            }
        }
    }
}