package per.d3249.learn.kotlin

import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon

data class Airport(
    @Json(name = "IATA") val code: String,
    @Json(name = "Name") val name: String,
    @Json(name = "Delay") val delay: Boolean
) {
    companion object {
        fun sort(unsortedList: List<Airport>): List<Airport> {
            return unsortedList.sortedBy { a -> a.name }
        }

        fun getAirportData(code: String) = try {
            Klaxon().parse<Airport>(fetchData(code)) as Airport
        } catch (ex: Exception) {
            Airport(code, "Invalid Airport", false)
        }


        fun fetchData(code: String) = java.net.URL("https://soa.smext.faa.gov/asws/api/airport/status/$code").readText()
    }
}