package per.d3249.learn.kotlin

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext

suspend fun getAirportStatus(codes: List<String>) = withContext(Dispatchers.IO) {
    Airport.sort(codes.map { async { Airport.getAirportData(it) } }.map { it.await() })
}

